# -*- coding: utf-8 -*-


import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.autograd import Variable

class tagging_loss(nn.Module):

    def __init__(self):
        super(tagging_loss, self).__init__()
        self.loss = nn.NLLLoss()

    def forward(self,prob_pred_list,tags):
        losses = []
        for prob,tag in zip(prob_pred_list,tags):
            losses += [self.loss(prob,tag)]

        # Average loss over sequence
        loss_matrix = torch.cat(losses,dim=0)
        ave_loss = torch.mean(loss_matrix,dim=0)

        return ave_loss

    def backward(self,grad_output): 
        return grad_output, None

class orthogonality_loss(nn.Module):

    def __init__(self):
        super(orthogonality_loss, self).__init__()
        a = None

    def forward(self,word_embs_src, word_embs_shd):
        
        # Iterate word embs
        P = []
        for word_emb_src,word_emb_shd in zip(word_embs_src,word_embs_shd):
            word_emb_src = word_emb_src.transpose(0,1).unsqueeze(0)
            word_emb_shd = word_emb_shd.unsqueeze(0)
            p_i = torch.bmm(word_emb_shd,word_emb_src)
            P.append(p_i)
        P = torch.cat(P,dim=0)
        return torch.sum(P,dim=0)

    def backward(self,grad_output): 
        return grad_output, None 






