# -*- coding: utf-8 -*-

import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.autograd import Variable

import opts

# Get options
DATA = opts.options.data
TOTAL_EPOCHS = opts.options.epochs
BATCH = opts.options.batch
MAX_LEN = opts.options.max_len
PRINT_EVERY = opts.options.print_every
LR = opts.options.lr
EMB = opts.options.emb
EDIM = opts.options.edim
ATT = opts.options.att

use_cuda = torch.cuda.is_available()



class Embeddings(nn.Module):
    r"""
    This is the customized embedding layer. 
    It encodes character-based words, and the entire sentence.
 
    output : [seq_len, batch, hidden_size]

    """
    def __init__(self, char_vocab_size, char_hidden_size,
                       word_vocab_size, word_hidden_size):
        super(Embeddings, self).__init__()

        print('char_vocab_size',char_vocab_size)
        print('char_hidden_size',char_hidden_size)
        print('word_vocab_size',word_vocab_size)
        print('word_hidden_size',word_hidden_size)

        # Init. hidden state
        self.n_layers = 1
        self.num_directions = 2
        self.hidden_c = Variable(torch.zeros(self.n_layers*self.num_directions, 
                                             BATCH, 
                                             char_hidden_size),
                                             requires_grad=True)
        self.hidden_w = Variable(torch.zeros(self.n_layers*self.num_directions, 
                                             BATCH, 
                                             (word_hidden_size+char_hidden_size)),
                                             requires_grad=True)

        # Embeddings
        self.char_embedding = nn.Embedding(char_vocab_size, char_hidden_size)
        self.word_embedding = nn.Embedding(word_vocab_size, word_hidden_size)

        # RNNs
        self.char_rnn = nn.GRU(char_hidden_size, 
                               char_hidden_size, 
                               bidirectional=True)
        self.word_rnn = nn.GRU(word_hidden_size+char_hidden_size*2, 
                               word_hidden_size+char_hidden_size, 
                               bidirectional=True)

    def forward(self, word_chars, words):
        # chars : [sent_len,word_len]
        # word : [sent_len]

        # Iterate words
        word_embs = []
        for word_char,word in zip(word_chars,words):
            char_emb = self.char_embedding(word_char).unsqueeze(1) # [word_len, batch, c_dim]
            char_output, hidden_c = self.char_rnn(char_emb, self.hidden_c) # [sent_len, word_len, c_dim]
            word_emb = self.word_embedding(word) # [sent_len, w_dim]

            # Concat
            v_i = torch.cat((hidden_c.view(1,1,-1),word_emb.unsqueeze(1)),dim=2)
            word_embs.append(v_i)

        word_embs = torch.cat(word_embs,dim=0) # [ sent_len, batch, dim ]
        sent_output, sent_emb = self.word_rnn(word_embs,self.hidden_w)
        return word_embs,sent_emb

class taggingModel(nn.Module):
    r"""
    This is the customized slot tagging layer. 
    It includes a single-layer feedforward network.

    input : [1, batch, num_feats]
    output : [1, batch, num_tag]

    """
    def __init__(self, num_feats, num_tag):
        super(taggingModel, self).__init__()

        self.num_feats = num_feats
        self.num_tag = num_tag

        self.layer1 = nn.Linear(num_feats*2, num_tag*2)
        self.layer2 = nn.Linear(num_tag*2, num_tag)

        self.softmax = nn.LogSoftmax(dim=1)

    def forward(self, word_embs_src, word_embs_shd):
        # word_embs_x : [ 1, batch, num_feats ]

        pred_prob_list, pred_tag_list = [], []

        # Iterate Hs
        for word_emb_src,word_emb_shd in zip(word_embs_src,word_embs_shd):

            # Concat
            h_i = torch.cat((word_emb_src,word_emb_shd),dim=1)
            output1 = self.layer1(h_i)
            g_t = self.layer2(output1)

            # Pred
            pred_prob = self.softmax(g_t)
            pred_prob_list.append(pred_prob)

            # Get label
            maxs, pred_label = torch.max(torch.exp(pred_prob),1)
            pred_tag_list.append(pred_label[0])            

        return pred_prob_list,pred_tag_list


class intentModel(nn.Module):
    r"""
    This is the customized intent classification layer. 
    It includes a single-layer feedforward network.

    input : [1, batch, num_feats]
    output : [1, batch, num_intent]

    """
    def __init__(self, num_feats, num_intent):
        super(intentModel, self).__init__()

        self.num_feats = num_feats
        self.num_intent = num_intent

        # Define layers
        self.mlp = nn.Linear(num_feats, num_intent)
        self.softmax = nn.LogSoftmax(dim=1)

        # Attention vector
        self.attention = Variable(torch.randn(1,num_feats),
                                  requires_grad=True)

    def forward(self, Hs):
        # Hs : [sent_len, batch, num_feats]

        if ATT:
            # Attention mechanism
            attVec = self.attention.repeat(1,Hs.size()[0],1)  # [batch, sent_len, dim]
            association = torch.bmm(attVec,Hs.permute(1,2,0))  # [batch, sent_len, dim] * [batch, dim, sent_len]
            alpha = F.softmax(association,dim=2)
            output = torch.sum(alpha,dim=2).unsqueeze(0)
            Hs = Hs.permute(1,0,2)
            Hs = torch.bmm(output,Hs)

        Hs_sum = torch.sum(Hs,dim=0)
        u_d = self.mlp(Hs_sum) # [ batch, num_intent ]
        maxs, pred_intent = torch.max(torch.exp(u_d),1)
        return self.softmax(u_d),pred_intent

class NonAdvdomainModel(nn.Module):
    r"""
    This is the customized domain classification layer. 
    It includes a single-layer feedforward network.

    output : [1, batch, num_domain]

    """
    def __init__(self, num_feats, num_domain):
        super(NonAdvdomainModel, self).__init__()

        self.num_feats = num_feats
        self.num_domain = num_domain

        # Define layers
        self.layer1 = nn.Linear(num_feats*2, num_domain)
        self.layer2 = nn.Linear(num_domain, num_domain)
        self.softmax = nn.LogSoftmax(dim=1)

        # Attention vector
        self.attention = Variable(torch.randn(1,num_feats*2),
                                  requires_grad=True)

    def forward(self, word_embs_src, word_embs_shd):
        # Hs : [1, batch, num_feats]

        Hs = []

        # Iterate Hs
        for word_emb_src,word_emb_shd in zip(word_embs_src,word_embs_shd):
            # Concat
            h_i = torch.cat((word_emb_src,word_emb_shd),dim=1)
            Hs.append(h_i.unsqueeze(0))

        Hs = torch.cat(Hs,dim=0)

        if ATT:
            # Attention mechanism
            attVec = self.attention.repeat(1,Hs.size()[0],1)  # [batch, sent_len, dim]
            association = torch.bmm(attVec,Hs.permute(1,2,0))  # [batch, sent_len, dim] * [batch, dim, sent_len]
            alpha = F.softmax(association,dim=2)
            output = torch.sum(alpha,dim=2).unsqueeze(0)
            Hs = Hs.permute(1,0,2)
            Hs = torch.bmm(output,Hs)


        Hs_sum = torch.sum(Hs,dim=0)
        u_d = self.layer1(Hs_sum) # [ batch, num_domain ]
        u_d = F.tanh(u_d)
        u_d = self.layer2(u_d)

        maxs, pred_domain = torch.max(torch.exp(u_d),1)
        return self.softmax(u_d),pred_domain

class domainModel(nn.Module):
    r"""
    This is the customized domain classification layer. 
    It includes a single-layer feedforward network.

    output : [1, batch, num_domain]

    """
    def __init__(self, num_feats, num_domain):
        super(domainModel, self).__init__()

        self.num_feats = num_feats
        self.num_domain = num_domain

        # Define layers
        self.layer1 = nn.Linear(num_feats, num_domain)
        self.layer2 = nn.Linear(num_domain, num_domain)
        self.softmax = nn.LogSoftmax(dim=1)

        # Attention vector
        self.attention = Variable(torch.randn(1,num_feats),
                                  requires_grad=True)

    def forward(self, Hs):
        # Hs : [1, batch, num_feats]

        if ATT:
            # Attention mechanism
            attVec = self.attention.repeat(1,Hs.size()[0],1)  # [batch, sent_len, dim]
            association = torch.bmm(attVec,Hs.permute(1,2,0))  # [batch, sent_len, dim] * [batch, dim, sent_len]
            alpha = F.softmax(association,dim=2)
            output = torch.sum(alpha,dim=2).unsqueeze(0)
            Hs = Hs.permute(1,0,2)
            Hs = torch.bmm(output,Hs)

        Hs_sum = torch.sum(Hs,dim=0)
        u_d = self.layer1(Hs_sum) # [ batch, num_domain ]
        u_d = F.tanh(u_d)
        u_d = self.layer2(u_d)
        maxs, pred_domain = torch.max(torch.exp(u_d),1)
        return self.softmax(u_d),pred_domain

class decoder(nn.Module):
    r"""
    Decoder that takes as input.
    Features:
        Attention mechanism

    """
    def __init__(self, hidden_size, word_vocab_size):
        super(decoder, self).__init__()

        self.n_layers = 1
        self.hidden_size = hidden_size

        # RNN
        self.gru = nn.GRU(hidden_size, hidden_size, num_layers=1)
        self.hiddenVec = self.initHidden()

        self.out = nn.Linear(hidden_size, word_vocab_size)
        self.softmax = nn.LogSoftmax(dim=1)

        # Attention vector
        self.attention = Variable(torch.randn(1,hidden_size,1),
                                  requires_grad=True)

    def forward(self, word_embs_src, word_embs_shd):
        r"""
            Reconstruct word sequence
        """

        Hs = []

        # Iterate Hs
        for word_emb_src,word_emb_shd in zip(word_embs_src,word_embs_shd):
            # Concat
            h_i = torch.cat((word_emb_src,word_emb_shd),dim=1)
            Hs.append(h_i.unsqueeze(0))

        # RNN
        Hs = torch.cat(Hs,dim=0)
        sent_output, sent_emb = self.gru(Hs,self.hiddenVec)
        
        # Attention
        association = torch.bmm(sent_output.transpose(0,1),self.attention)  # [batch, sent_len, dim] * [batch, dim, sent_len]
        alpha = F.softmax(association,dim=0)   
        o1 = alpha.view(-1).data[0] * sent_output.transpose(0,1)
        output = self.out(o1)
        pred_prob = self.softmax(output) # [ batch, sent_len, dim ]

        # Prediction        
        maxs, pred_labels = torch.max(torch.exp(pred_prob),dim=2)

        return pred_prob.squeeze(0) # [ 1, sent_len ]
 
    def initHidden(self):
        result = Variable(torch.zeros(self.n_layers, 1, self.hidden_size))
        if use_cuda:
            return result.cuda()
        else:
            return result


