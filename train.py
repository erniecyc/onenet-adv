# -*- coding: utf-8 -*-

from init import *


def main(): 

    # Prepares data
    io = IO.IO()
    io.prepareData(DATA)

    # Init. models
    models = initModels(io)

    # Init. trainer
    trainer(io, models, total_epochs=TOTAL_EPOCHS)


def trainer(io, models, total_epochs=1000):
    r"""  
    This function defines the training procedure.
    
    Args: 
        io(IO): IO object.
        domain(domainModel): domainModel object.
        intent(intentModel): intentModel object.
        tag(taggingModel): taggingModel object.
        total_epochs(int): number of epochs.

    Returns:
        None

    """ 

    # Get models
    onenet_optimizer = optim.Adam(list(models['domain'].parameters())+ \
                                  list(models['tag'].parameters()) + \
                                  list(models['intent'].parameters()) + \
                                  list(models['sentEmb_src'].parameters()) + \
                                  list(models['sentEmb_shd'].parameters()) + \
                                  list(models['decoder'].parameters())
                                  ,lr=LR)

    # Prepare configs
    criterion = nn.NLLLoss()
    tag_loss = l.tagging_loss()
    ortho_loss = l.orthogonality_loss()

    start_epoch = 1
    data_size = max(int(io.datasize/BATCH)+1,2)
    b = IO.batch(io,batch=BATCH,ratio=1)
    y_trues, y_preds = [], []

    print('Total data points {}.'.format(io.datasize))
    print('Batch size {}.'.format(BATCH))

    for epoch in range(start_epoch,total_epochs+1):

        print('Epoch {}/{}'.format(epoch,total_epochs))

        start = time.time()
        plot_losses = []
        print_loss_total = 0

        progress = 0
        while b.hasNext():
            loss,y_true,y_pred = step(io, b, models, onenet_optimizer, criterion, tag_loss, ortho_loss)
            print_loss_total += loss
            progress += 1
            y_trues += y_true
            y_preds += y_pred

        # Reset
        b.reset()

        # Compute F1 loss
        from sklearn.metrics import precision_recall_fscore_support
        f1 = precision_recall_fscore_support(y_trues, y_preds, average='micro')[2]

        # Report
        print_loss_avg = print_loss_total / data_size
        print( str('%s (%d %d%%) loss %.4f F1 %.4f' % (report.timeSince(start, progress / data_size),
                                     progress, progress / data_size * 100, print_loss_avg, f1))
                                     .center(os.get_terminal_size().columns) )

        if f1>0.5:
            checkpoint = {
                'opt': opts.options,
                'epoch': epoch,
                'io': io,
                'models': models,
                'onenet_optimizer': onenet_optimizer
            }
            if ATT:
                torch.save(checkpoint,open(opts.options.model+'/model_ATT_'+str(round(f1,3)),'wb'))
            else:
                torch.save(checkpoint,open(opts.options.model+'/model_'+str(round(f1,3)),'wb'))
        

def step(io, b, models, optimizer, criterion, tag_loss, orthogonality_loss):
    r"""
    This function steps through the models.

    Returns:
        ave_loss(int): average loss over sequence length.
    """

    # Build word representation
    word_embs_src,sent_emb_src = models['sentEmb_src'](b.charVars,b.wordVar)
    word_embs_shd,sent_emb_shd = models['sentEmb_shd'](b.charVars,b.wordVar)

    # Predict Intent Feed in target domain test data!
    pred_intent_prob,pred_intent = models['intent'](word_embs_shd)

    ######################## Calculate Loss #################################

    # Source side tagging loss
    pred_prob_list,pred_tag_list = models['tag'](word_embs_src,word_embs_shd)

    # Reconstruction loss
    pred_labels = models['decoder'](word_embs_src,word_embs_shd)

    # Adversarial Domain Classification Loss
    rand_class = np.array([random.choice(list(range(io.num_domain)))])
    rand_label = Variable(torch.LongTensor(rand_class))
    # 0.5 chance
    label_choice = random.choice(list(range(1)))
    if label_choice:
        randomDomainVar = rand_label 
    else:
        randomDomainVar = b.domainVar
    pred_domain_prob_adv,pred_domain_adv = models['domain'](word_embs_shd)

    # Non-adversarial Domain Classification Loss
    pred_domain_prob,pred_domain = models['NonAdv_domainM'](word_embs_src,word_embs_shd)

    # Compute loss
    loss =  tag_loss(pred_prob_list,b.tagVar) + \
            criterion(pred_labels,b.wordVar) + \
            orthogonality_loss(word_embs_src,word_embs_shd) + \
            criterion(pred_domain_prob,b.domainVar) + \
            criterion(pred_domain_prob_adv,randomDomainVar)

    # Get predictions for F1
    true_tag_list = [ tag for tag in b.tagVar.data ]
    pred_tag_list = [ tag.data[0] for tag in pred_tag_list ]
    y_true = [ b.intentVar.data[0], b.domainVar.data[0] ]+true_tag_list
    y_pred = [ pred_intent.data[0], pred_domain.data[0] ]+pred_tag_list

    # Backprop
    loss.backward()

    # Step optimizer (change lr)
    optimizer.step()

    return loss,y_true,y_pred


if __name__ == "__main__":
    import time
    start = time.time() 
    main()
    print("Time taken {}".format(time.time()-start))




