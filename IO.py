# -*- coding: utf-8 -*-

import unicodedata
from io import open
import unicodedata
import string
import re

import torch
from torch.autograd import Variable as Variable
import numpy as np

from Vocab import wordVocab
from Vocab import charVocab
from Vocab import labelVocab
import opts as opts


dataPath = opts.options.data
max_len = opts.options.max_len
EMB = opts.options.emb
use_cuda = torch.cuda.is_available()

EPATH = opts.options.epath
EMB = opts.options.emb
EDIM = opts.options.edim
num_embeddings = 0

def compat_splitting(line):
    return line.decode('utf8').split()

class batch:
    """
    This class prepares batch.

    """
    def __init__(self,io,batch=32,ratio=0.01,max_length=150):
        self.io = io
        self.ratio = ratio
        self.batch = batch
        self.done = False
        self.max_length = max_length
        self.wordVar = None
        self.charVars = None 
        self.intentVar = None
        self.tagVar = None
        self.domainVar = None
        self.iter = self.iterbatch()

    def getData(self,pairs):
        self.io.pairs = pairs

    def reset(self):
        self.iter = self.iterbatch()
        self.done = False

    def hasNext(self):
        try:
            # Load data
            self.wordVar, self.charVars, self.intentVar, self.tagVar, self.domainVar \
                            = self.iter.__next__()
        except StopIteration as e:
            self.done = True
        return not(self.done)

    def pad(self,l):
        """ Pad sequence """
        for e in l:
            # Padding
            e_ = torch.zeros(self.max_length, encoder.hidden_size)
            e += e_ 

    def iterbatch(self):
        """
        This class make batches of data available.

        """
        import random
        data_size = int(self.io.datasize*self.ratio)
        training_pairs = [ self.io.pair2Variables(pair) for pair in self.io.pairs ]
        random.shuffle(training_pairs) # Shuffle training set
        # Getting...
        for ndx in range(0, data_size, self.batch):
            wordVar, charVars, intentVar, tagVar, domainVar = \
                    training_pairs[ndx]
            yield wordVar, charVars, intentVar, tagVar, domainVar

class IO:
    """
    This class prepares data.

    """
    def __init__(self):
        self.char_vocab = None 
        self.word_vocab = None
        self.pairs = None
        self.SOS_token = 0
        self.EOS_token = 1
        self.UNK = 'UNK'
        self.datasize = 0
        self.max_len = 0

        self.num_domain = 0
        self.num_tag = 0
        self.num_intent = 0

    def cleanData(self,pairs):
        new_pair = []
        max_len = 25
        for pair in pairs: 
            sent1 = pair[0].split(' ')
            sent2 = pair[1].split(' ')
            if len(sent1)>max_len:
                sent1 = sent1[:max_len]
            if len(sent2)>max_len:
                sent2 = sent2[:max_len]
            new_pair.append( (' '.join(sent1),' '.join(sent2)) )
        return new_pair

    def preprocessTokens(self,utterance):
        """ Preprocess sentence """
        new_utterance = []
        for token in utterance.split():
            new_utterance.append(self.normalizeString(token))
        return new_utterance

    def extractTagsAndTokens(self,data):
        """ Function to extract tags """

        extracted_tokens = list(set(re.findall(r'>([^<>]*)</',data)))
        extracted_tags = [ tag for tag in \
                        set(re.findall(r'<([^<>]*)>',data)) if '/' not in tag]
        utterance,tag = [],[]
        tag_index = 0
        tag_flag = False

        for token in data.split():

            # Reset flag
            if tag_flag and '/>' not in token: 
                continue
            else:
                tag_flag = False

            if '<' in token:
                tag_flag = True
                utterance.append(extracted_tokens[tag_index].lower())
                tag.append(extracted_tags.pop())
                tag_index += 1
            else:
                utterance.append(token.lower())
                tag.append('NULL')

        return utterance,tag

    def initVocabs(self):
        self.intent_vocab = labelVocab('intent')
        self.domain_vocab = labelVocab('domain')
        self.tag_vocab = labelVocab('tags')
        self.intent_vocab.addLabel(self.intent)
        self.domain_vocab.addLabel(self.domain)
        self.tag_vocab.addLabel(self.tags)

    def readFromData(self,data_path):
        """ Read from HW """

        pairs = []
        tag_data,intent_data,domain_data = [],[],[]

        for line in open(data_path,'r').readlines()[1:]:

            cols = line.replace('\n','').split('\t')
            _,tag,intent,domain = cols[2],cols[3],cols[4],cols[5]

            utterance,tag = self.extractTagsAndTokens(tag)

            pairs.append((utterance,tag,intent,domain))

            tag_data.append(' '.join(tag))
            intent_data.append(intent)
            domain_data.append(domain)

        # Get labels
        self.tags = list(set(' '.join(tag_data).split()))
        self.intent = list(set(intent_data))
        self.domain = list(set(domain_data))
        self.initVocabs()

        # Init model parameters
        self.num_tag = len(self.tags)
        self.num_domain = len(self.domain)
        self.num_intent = len(self.intent)

        return pairs

    def prepareData(self,data_path='datasets/train'):

        print("Reading lines...")
        self.pairs = self.readFromData(data_path)

        # Truncate to length 25
        #pairs = self.cleanData(pairs)

        print("Read %s sentence" % len(self.pairs))
        print("Creating vocab...")

        # Creating vocab
        char_vocab = charVocab('char')
        word_vocab = wordVocab('word')

        for pair in self.pairs:
            char_vocab.addSentence(pair[0])
            word_vocab.addSentence(pair[0])

        self.max_len = word_vocab.max_len

        print("Total vocab size:",word_vocab.n_words)
        print("Maximum length:",self.max_len)

        self.char_vocab = char_vocab
        self.word_vocab = word_vocab
        self.datasize = len(self.pairs)

    def utterance2Vars(self,utterance):
        """ Convert utterance to embeddings """
        utterance = self.preprocessTokens(utterance)
        charVars = self.toVars(self.char_vocab, utterance)
        wordVar = self.toVars(self.word_vocab, utterance)
        return (wordVar, charVars)

    def pair2Variables(self, pair):
        """ Convert pairs of data to variable objects """

        utterance,tag,intent,domain = pair

        charVars = self.toVars(self.char_vocab, utterance)
        wordVar = self.toVars(self.word_vocab, utterance)
        intentVar = self.toVars(self.intent_vocab, intent)
        tagVar = self.toVars(self.tag_vocab, tag)
        domainVar = self.toVars(self.domain_vocab, domain)

        return (wordVar, charVars, intentVar, tagVar, domainVar)

    def toVars(self, vocab, raw_input):
        indices = self.sentence2Indices(vocab, raw_input)
        result = Variable(torch.LongTensor(indices))
        if use_cuda:    
            return result.cuda() 
        else:
            return result

    def sentence2Indices(self, vocab, raw_input):
        if vocab.name in 'tags' or vocab.name in 'intent' or vocab.name in 'domain':
            if type(raw_input)==type(' '):
                raw_input = [raw_input]
            indices = [vocab.label2index[label] for label in raw_input]
            return indices
        if vocab.name in 'char':
            indices = [self.pad_indices(15,[vocab.char2index[c] \
                        for c in word.replace(' ','')],0) \
                        for word in raw_input]
            return indices
        if vocab.name in 'word':
            indices = [vocab.word2index[word] if word in vocab.word2index.keys() 
                        else vocab.word2index[self.UNK] for word in raw_input]
            return indices

    def to_one_hot(self, label, vocab):
        """ 
        Take integer y (tensor or variable) 
        with n dims and convert it to 1-hot 
        representation with n+1 dims. 
        """
        idx = vocab.label2index[label]
        dim = len(vocab.label2index.keys())
        one_hot = [0]*dim
        one_hot[idx-1] = 1
        return Variable(torch.LongTensor(one_hot))

    def unicodeToAscii(self,s):
        return ''.join(
            c for c in unicodedata.normalize('NFD', s)
            if unicodedata.category(c) != 'Mn'
        )

    # Lowercase, trim, and remove non-letter characters
    def normalizeString(self,s):
        s = self.unicodeToAscii(s.lower().strip())
        s = re.sub(r"([.!?])", r" \1", s)
        s = re.sub(r"[^a-zA-Z.!?]+", r" ", s)
        exclude = set(string.punctuation)
        s = ''.join(ch for ch in s if ch not in exclude)
        return s

    def pad_indices(self,max_length,original,pad_val):
        padded_z = [pad_val]*max_length
        for ei in range(max_length):
            if ei>len(original)-1: break
            padded_z[ei] = original[ei]
        return padded_z


# For testing 
if __name__ == "__main__":

    # Init io
    io = IO()
    # Prepares data
    io.prepareData()

    # Test cases
    print(io.num_domain,io.num_intent,io.num_tag)










