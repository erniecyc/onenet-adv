# -*- coding: utf-8 -*-

from __future__ import unicode_literals, print_function, division
import random
import os
import argparse

import torch
import torch.nn as nn
from torch.autograd import Variable
from torch import optim



class generator(object):
    r"""
    generator class to generate intents, tags, and domains.
    
    """
    def __init__(self,checkpoint='saved_models/model_99'):
        super(generator, self).__init__()
        """ Load models and configs """
        checkpoint = torch.load(checkpoint)
        self.models = checkpoint['models']

        self.models['intent'].eval()
        self.models['domain'].eval()
        self.models['tag'].eval()

        self.io = checkpoint['io']
        self.opt = checkpoint['opt']

    def getPred(self,utterance=None,b=None):
        """ Get predictions """

        io = self.io
        models = self.models

        if utterance:
            # Convert utterance to Vars
            wordVar, charVars = io.utterance2Vars(utterance)  
        else:
            wordVar, charVars = b.wordVar, b.charVars 

        # Build word representation
        word_embs,sent_emb = models['word_emb'](charVars,wordVar)

        # Make 3-way predictions
        pred_intent_prob,pred_intent = models['intent'](word_embs)
        pred_domain_prob,pred_domain = models['domain'](word_embs)
        pred_prob_list,pred_tag_list = models['tag'](word_embs)

        intent = io.intent_vocab.index2label[pred_intent.data[0]]
        domain = io.domain_vocab.index2label[pred_domain.data[0]]
        tag_list = [ io.tag_vocab.index2label[tag.data[0]] for tag in pred_tag_list ]

        # Get predictions for F1        
        pred_tag_list = [ tag.data[0] for tag in pred_tag_list ]
        y_pred = [ pred_intent.data[0], pred_domain.data[0] ]+pred_tag_list

        pred_dict = {
            'domain': domain,
            'intent': intent,
            'tags': tag_list,
            'y_pred': y_pred,
            'y_intent_pred': [pred_intent.data[0]],
            'y_domain_pred': [pred_domain.data[0]],
            'y_tag_pred': pred_tag_list
        }
        #print(pred_dict)
        return pred_dict 

    def prepareLabels(self,b=None):
        # Get predictions for F1
        true_tag_list = [ tag for tag in b.tagVar.data ]
        y_true = [ b.intentVar.data[0], b.domainVar.data[0] ]+true_tag_list
        true_dict = {
            'y_true': y_true,
            'y_intent_true': [b.intentVar.data[0]],
            'y_domain_true': [b.domainVar.data[0]],
            'y_tag_true': true_tag_list
        }
        return true_dict

def main():
    g = generator(generate_options.checkpoint)
    utterance = 'How are you?'
    print(g.getPred(utterance))


if __name__ == "__main__":
    import time
    start = time.time() 
    main()
    print("Time taken {}".format(time.time()-start))






