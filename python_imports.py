# -*- coding: utf-8 -*-

from __future__ import unicode_literals, print_function, division
import random
import os

import numpy as np
import torch
import torch.nn as nn
from torch.autograd import Variable
from torch import optim
import torch.nn.functional as F

import IO
import opts
import Models.Loss as l
import Models.Models as m
import report
import opts



# Get options
DATA = opts.options.data
TOTAL_EPOCHS = opts.options.epochs
BATCH = opts.options.batch
MAX_LEN = opts.options.max_len
PRINT_EVERY = opts.options.print_every
LR = opts.options.lr
EMB = opts.options.emb
EDIM = opts.options.edim
ATT = opts.options.att


use_cuda = torch.cuda.is_available()

