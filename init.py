# -*- coding: utf-8 -*-

from python_imports import *

def initModels(io):
    """ To initialize all models """

    # Init. models
    c_dim = 25
    w_dim = 100
    num_feats = c_dim*2+w_dim

    # Init. models
    domainM = m.domainModel(num_feats, io.num_domain)
    NonAdv_domainM = m.NonAdvdomainModel(num_feats, io.num_domain)
    intentM = m.intentModel(num_feats, io.num_intent)
    tagM = m.taggingModel(num_feats, io.num_tag)
    decoder = m.decoder(hidden_size=num_feats*2,
                        word_vocab_size=io.word_vocab.n_words)

    # Init both embeddings
    sentEmb_src = m.Embeddings(char_vocab_size=io.char_vocab.n_words, 
                               char_hidden_size=c_dim, 
                               word_vocab_size=io.word_vocab.n_words, 
                               word_hidden_size=w_dim)
    sentEmb_shd = m.Embeddings(char_vocab_size=io.char_vocab.n_words, 
                               char_hidden_size=c_dim, 
                               word_vocab_size=io.word_vocab.n_words, 
                               word_hidden_size=w_dim)

    if use_cuda:
        domain = domain.cuda()
        intent = intent.cuda()
        tag = tag.cuda()

    models = {'domain':domainM,
              'NonAdv_domainM':NonAdv_domainM,
              'intent':intentM,
              'tag':tagM,
              'sentEmb_src':sentEmb_src,
              'sentEmb_shd':sentEmb_shd,
              'decoder':decoder}

    return models








