sessionID	turnID			utterance
0	1	inform			<pickup_delivery>take-out</pickup_takeout> will be great, please
1	1	inform			<pickup_delivery>pick up</pickup_delivery> from your georgetown location
2	1	inform			<user_phone>360-555-7000</user_phone>
3	1	inform			<pickup_delivery>take out</pickup_delivery> sounds good
4	1	reorder		can i reorder my again, please
5	1	reorder		may i order my <pizza_type>previous pizza</pizza_type> order
6	1	reorde			please reorder my <pizza_type>most recent pizza</pizza_type> again
7	1	order			may i get a <pizza_size>small</pizza_size> <pizza_type>vegetarian</pizza_type>, please
8	1	order			can i order a all <pizza_type>meat</pizza_type> on <pizza_crust>thin</pizza_crust>. make it <pizza_size>small</pizza_small>, please
9	1	reorder		i'd like to place another order for my <pizza_type>most common</pizza_type>
10	1	inform			it's michael wait <user_name>danko black</user_name>
11	1	goodbye		terrific thanks
12	1	order			may i get a <pizza_type>5 cheese</pizza_type>, please. make it <pizza_size>small</pizza_size>
13	1	reorder, inform	i'd like to get my <pizza_type>most recent</pizza_type> again, please
14	1	reorder, inform	can i place an order for my <pizza_type>most frequent</pizza_type> order
15	1	deny			nope
16	1	inform			i'll have a <pizza_size>14 inch</pizza_size> <pizza_type>hawaiian</pizza_type>
17	1	order, inform		hey, how's it going? i'd like a <pizza_size>large</pizza_size> <pizza_type>4 cheese</pizza_type>, please
18	1	inform			<pickup_delivery>pick up</pickup_delivery> will be great
19	1	goodbye		terrific thank you
20	1	reorder, inform	reorder my <pizza_type>most common</pizza_type> again
21	1	inform			oh, and can you, please make it <pickup_delivery>take-out</pickup_delivery>
22	1	reorder, inform	please take my <pizza_type>most recent<\pizza_type> order again
23	1	query_status		when will my order get here?
24	1	reorder, inform	may i reorder my <pizza_type>preferred</pizza_type> pizza
25	1	reorder, inform	<pizza_type>reorder</pizza_type> my previous pizza
26	1	order, inform		hello, can i get a <pizza_type>4 cheese</pizza_type> ?
27	1	query_status		when do you think my order will be ready?
28	1	inform			<user_phone>425-555-5990</user_phone>
29	1	confirm		yeah, to go will be great
30	1	order, inform		hi, i'll have a <pizza_size>small</pizza_size>  <pizza_type>vegan</pizza_type>
31	1	inform			use <user_name>logan</user_name>, please
32	1	order, inform		i'd like a <pizza_size>large</pizza_size> <pizza_type>veggie supreme</pizza_type>
33	1	confirm		yeah, pick up will be great, please
34	1	hello			hello,
35	1	confirm		that would be great yep,
36	1	order, inform		i'd like to get a <pizza_type>hawaiian</pizza_type>. make it <pizza_size>large</pizza_size>
37	1	query_status		how long until my pizza is ready?
38	1	query_status		has my order left from your store yet?
39	1	reorder, inform	may i reorder my <pizza_type>favorite</pizza_type>
40	1	order, inform 		i'd like a <pizza_size>large</pizza_size> <pizza_type>hawaiian</pizza_type>, please
41	1	confirm, inform 	yes, how about <pickup_delivery>delivery</pickup_delivery>
42	1	inform 		oh, and can you make it <pickup_delivery>pick up</pickup_delivery>
43	1	change		i'd like a different pizza
44	1	order, inform 		i'd like to order my <pizza_type>previous</pizza_type> pizza
45	1	query_status		how long until my order is delivered?
46	1	order, inform		may i get a <pizza_type>vegan</pizza_type>, please
47	1	reorder, inform	could i take my <pizza_type>most recent</pizza_type> pizza order again
48	1	reorder, inform	may i get my <pizza_type>preferred</pizza_type>
49	1	reorder, inform 	may i reorder my <pizza_type>most frequent</pizza_type> pizza order again
50	1	goodbye		awesome thank you
51	1	order, inform		hi, may i get a <pizza_size>medium<pizza_size> <pizza_type>hawaiian</pizza_type>, please
52	1	goodbye		terrific thank you
53	1	inform 		<pickup_delivery>pick up</pickup_delivery> from your <location>fremont</location> location sounds good
54	1	inform			it's rahul wait <user_name>gabrielle brown</user_name>
55	1	order, inform	 	may i get a <pizza_type>meat lovers</pizza_type>. make it <pizza_size>large</pizza_size>
56	1	order, inform		i'd like a <pizza_type>vegetarian</pizza_type>
57	1	inform			it's <user_phone>360-555-1271</user_phone>
58	1	inform, confirm 	yep, oh, and can you make it <pickup_delivery>take out</pickup_delivery> from your <location>bellevue</location> location, please
59	1	confirm	 	yep, pick up will be great
60	1	reorder, inform	can i place another order for my <pizza_type>most recent</pizza_type> order, please
61	1	order, inform		give me a uhm <pizza_size>small</pizza_size> the <pizza_type>meats</pizza_type> on <pizza_crust>thin</pizza_crust>
62	1	change		let's go with a <pizza_size>12 inch</pizza_size> instead, please
63	1	deny, change		no, i'd like to change something
64	1	inform			let's go with <user_name>lissa</user_name>
65	1	order, inform		hey, how's it going? may i get a <pizza_size>large</pizza_size> <pizza_type>pepperoni</pizza_type>, please
66	1	inform			i'd like a <pizza_type>meat lovers</pizza_type>
67	1	deny			no
68	1	reorder, inform	place another order for my <pizza_type>previous</pizza_type> order
69	1	deny, change		nope, i'd like to change something
70	1	reorder, inform	may i order my <pizza_type>most recent</pizza_type> pizza order again
71	1	query_status		how long until my order is ready?
72	1	confirm		yeah, absolutely
73	1	confirm		yeah, that would be great
74	1	order, inform		can i order my <pizza_type>most frequent</pizza_type> order again, please
75	1	inform			put it under <user_name>asli</user_name>
76	1	order, inform		hi, may i place an order for <pickup_delivery>take out</pickup_delivery>
77	1	deny			nope
78	1	inform			<user_name>alex</user_name>, please
79	1	confirm		absolutely yes,
80	1	confirm, inform	yes, how about <pickup_delivery>to go</pickup_delivery>, please
81	1	order, inform 		i'd like to get a <pizza_type>pepperoni</pizza_type> on <pizza_crust>thin crust</pizza_crust>. make it <pizza_size>small</pizza_size>, please
82	1	reorder, inform	please order my <pizza_type>favorite</pizza_type> order again
83	1	reorder, inform	can i place an order for my <pizza_type>previous</pizza_type> again
84	1	inform			it's imed wait <user_name>logan doe</user_name>
85	1	goodbye		amazing thanks
86	1	reorder, inform	may i reorder my <pizza_type>most recent</pizza_type> pizza order, please
87	1	deny, change		no, i'd like to change something
88	1	inform			i'd like to get a <pizza_type>5 cheese</pizza_type>. make it <pizza_size>14 inch</pizza_size>
89	1	order, inform		place an order for my <pizza_type>favorite</pizza_type> pizza order
90	1	inform			how about <pickup_delivery>take-out</pickup_delivery>
91	1	query_status		how long until my pizza order gets delivered?
92	1	reorder, inform	reorder my <pizza_type>preferred</pizza_type> pizza order again
93	1	confirm		, please yeah,
94	1	order, inform		hi, can i get a <pizza_type>4 cheese</pizza_type> ?
95	1	confirm		absolutely yeah,
96	1	order, inform		hello, can i get a <pizza_type>all the veggies</pizza_type>, please ?
97	1	goodbye		great thanks
98	1	inform			<user_phone>360-555-5552</user_phone>
99	1	confirm, inform	yeah, how about <pickup_delivery>take-out</pickup_delivery>
100	1	order, inform		i want to order a <pizza_type>veggie supreme</pizza_type> on <pizza_crust>gluten free</pizza_crust>, please
101	1	reorder, inform	may i reorder my <pizza_type>favorite</pizza_type> pizza
102	1	reorder, inform	may i reorder my <pizza_type>previous</pizza_type> pizza again, please
103	1	inform			it is <user_name>david</user_name>
104	1	deny, change		nope, i'd like to change something
105	1	reorder, inform	could i order my <pizza_type>most frequent</pizza_type> pizza again
106	1	change		change the pizza, please
107	1	order, inform		give me a <pizza_size>medium</pizza_size> <pizza_type>all the veggies</pizza_type> on <pizza_crust>gluten free</pizza_type> crust, please
108	1	order, inform		i want to order a <pizza_size>large</pizza_size> <pizza_type>4 cheese</pizza_type>, please
109	1	order, inform		may i get a <pizza_size>small</pizza_size> <pizza_type>veggie supreme</pizza_type>
110	1	hello			hello,
111	1	reorder, inform	may i place an order for my <pizza_type>previous</pizza_type>
112	1	order, inform		i'll have a uh <pizza_type>hawaiian</pizza_type>
113	1	inform			put it under <user_name>gabrielle</user_name>, please
114	1	order			hey, how's it going? i'd like to place an order
115	1	order, inform		i'll order a uh... <pizza_type>pepperoni</pizza_type> on <pizza_crust>gluten free</pizza_crust>
116	1	goodbye		great thank you
117	1	reorder, inform	reorder my <pizza_type>favorite</pizza_type> pizza order
118	1	confirm, inform	yes, <pickup_delivery>take out</pickup_delivery> from your <location>fall city square</location> location, please
119	1	order, inform		hi, can i get a <pizza_size>small</pizza_size> <pizza_type>meat lovers</pizza_type> ?
120	1	reorder, inform	please order my <pizza_type>usual</pizza_type> pizza
121	1	inform			it's <user_phone>206-555-5555</user_phone>
122	1	order, inform		hi, can i get a <pizza_size>small</pizza_size> <pizza_type>4 cheese</pizza_type> ?
123	1	inform			<user_phone>it's 206-555-8761</user_phone>
124	1	inform			<pickup_delivery>pick up</pickup_delivery> would be great, please
125	1	order, inform		i want a <pizza_type>all veggies</pizza_type>
126	1	order, inform		may i place an order for my <pizza_type>most recent</pizza_type>
127	1	inform			it's asli wait <user_name>shadi brown</user_name>
128	1	reorder, inform	i'd like to order my <pizza_type>usual</pizza_type> again
129	1	order, inform		hello, i'll have a <pizza_type>hawaiian</pizza_type>
130	1	order, inform		hello, may i get a <pizza_size>10 inch</pizza_size> <pizza_type>veggie supreme</pizza_type>, please
131	1	order, inform		hello, i'd like to place an order for <pickup_delivery>take out</pickup_delivery>
132	1	deny			no
133	1	hello			hello,
134	1	hello			hi,
135	1	query_status		has my order left from your store yet?
136	1	order, inform		i'll order a uhm... <pizza_type>pepperoni</pizza_type>. make it <pizza_size>10 inch</pizza_size>
137	1	inform			<user_phone>206-555-8712</user_phone>
138	1	deny			nope
139	1	deny			nope
140	1	inform			oh, and can you, please make it <pickup_delivery>delivery</pickup_delivery>, please
141	1	inform			<user_name>alexandre</user_name, please
142	1	reorder, inform	please reorder my <pizza_type>usual</pizza_type> pizza order again
143	1	reorder, inform	can i reorder my <pizza_type>previous</pizza_type>, please
144	1	change		no, i'd like to change something
145	1	reorder		can i reorder my pizza again
146	1	inform			<pickup_delivery>delivery</pickup_delivery> will be great, please
147	1	reorder, inform	can i place an order for my <pizza_type>usual</pizza_type> pizza order, please
148	1	deny, change		no, i'd like to change something
149	1	query_status		can you tell me where's my pizza ?
150	1	inform			oh, and can you make it <pickup_delivery>delivery</pickup_delivery>, please
151	1	goodbye		great thanks
152	1	inform			<pickup_delivery>pick up</pickup_delivery> would be great
153	1	confirm		yes, pick up sounds good
154	1	reorder, inform	please place an order for my <pizza_type>most common</pizza_type> pizza again
155	1	order, inform		hello, can i get a <pizza_type>meat lovers</pizza_type> ?
156	1	order, inform		hey, how's it going? may i get a <pizza_type>all the veggies</pizza_type>
157	1	reorder, inform	place an order for my <pizza_type>most recent</pizza_type> pizza again
158	1	inform			<pickup_delivery>take-out</pickup_delivery> sounds good
159	1	change		i want another crust type, please
160	1	inform			it's <user_name>elizabeth</user_name>
161	1	inform			please make it <pickup_delivery>pick up</pickup_delivery>
162	1	confirm, inform	yep, <pickup_delivery>take out</pickup_delivery> will be great, please
163	1	goodbye		terrific thank you
164	1	confirm		absolutely yep,
165	1	reorder, inform	could i reorder my <pizza_type>most common</pizza_type> pizza order
166	1	order, inform		place an order for my <pizza_type>most recent</pizza_type> order again
167	1	order, inform		may i get a <pizza_type>veggie supreme</pizza_type> on <pizza_size>thin</pizza_size>
168	1	inform			<pickup_delivery>take-out</pickup_delivery> will be great, please
169	1	reorder, inform	may i place another order for my <pizza_type>favorite</pizza_type> order again, please
170	1	query_status		can you tell me when my order will be delivered,, please ?
171	1	reorder, inform	may i place another order for my <pizza_type>most recent</pizza_type> pizza again
172	1	confirm, inform	yeah, oh, and can you, please make it <pickup_delivery>take out</pickup_delivery>
173	1	confirm		yeah, perfect
174	1	confirm		yeah,, please
175	1	inform			it's <user_name>paul</user_name>, please
176	1	reorder, inform	please order my <pizza_type>previous</pizza_type> pizza order
177	1	inform			<pickup_delivery>pick up</pickup_delivery> would be great, please
178	1	inform			how about <pickup_delivery>pick up</pickup_delivery>
179	1	order, inform		can i order a uhm... <pizza_size>small</pizza_size> <pizza_type>hawaiian</pizza_type> on <pizza_crust>regular</pizza_crust>
180	1	query_status		how long until my pizza is ready?
181	1	query_status		when will my order arrive here?
182	1	order, inform		give me a <pizza_type>no dairy no meat special</pizza_type>. make it <pizza_size>12 inch</pizza_size>, please
183	1	order, inform		can i order a uh... <pizza_type>pepperoni</pizza_type>, please
184	1	inform			oh, and can you make it <pickup_delivery>pick up</pickup_delivery>, please
185	1	inform			it's <user_phone>519-555-8896</user_phone>
186	1	order, inform		can i get a <pizza_type>hawaiian</pizza_type> on <pizza_crust>gluten free</pizza_crust>. make it <pizza_size>10 inch</pizza_size>, please
187	1	deny, change		nope, i'd like to change something
188	1	order, inform		i'd like to place an order for my <pizza_type>most recent</pizza_type> pizza again
189	1	order inform		can i order a uhm... <pizza_type>meat lovers</pizza_type> on <pizza_crust>regular</pizza_crust> crust. make it <pizza_size>10 inch</pizza_size>, please
190	1	inform			it is <user_name>eric</user_name>
191	1	order, inform		order my <pizza_type>most frequent</pizza_type> order
192	1	order, inform		i'll get a <pizza_type>4 cheese</pizza_type> on <pizza_crust>regular</pizza_crust>. make it <pizza_size>small</pizza_size>
193	1	reorder, inform	could i reorder my <pizza_type>most common</pizza_type> pizza order
194	1	order, inform		hey, how's it going? i'll have a <pizza_size>medium</pizza_size> <pizza_type>pepperoni</pizza_type>
195	1	inform			how about <pickup_delivery>pick up</pickup_delivery>, please
196	1	confirm, inform	yep, let's go with <pickup_delivery>delivery</pickup_delivery>
197	1	reorder, inform	i'd like to place another order for my <pizza_type>usual</pizza_type> pizza again, please
198	1	inform			<pickup_delivery>delivery</pickup_delivery> will be great
199	1	reorder, inform	please place another order for my <pizza_type>most common</pizza_type> again
200	1	order, inform		hello, can i get a <pizza_size>small</pizza_size> <pizza_type>hawaiian</pizza_type>, please ?
201	1	reorder, inform	please place an order for my <pizza_type>favorite</pizza_type> order
202	1	query_status		how long until my order gets delivered?
203	1	goodbye		great thank you
204	1	inform			it's <user_phone>206-555-8712</user_phone>
205	1	confirm		yep, take-out
206	1	deny, change		nope, i'd like to change something
207	1	change		actually i'd like to change the kind of crust
208	1	deny			nope
209	1	order, inform		hey, how's it going? i'd like a <pizza_size>large</pizza_size> <pizza_type>hawaiian</pizza_type>, please
210	1	confirm		yeah,
211	1	order, inform		hey, how's it going? can i get a <pizza_type>all meat</pizza_type> special, please ?
212	1	order, inform		i'd like to order a <pizza_size>12 inch</pizza_size> <pizza_type>vegan</pizza_type> on <pizza_crust>gluten free</pizza_crust> crust
213	1	reorder, inform	place another order for my <pizza_type>most frequent</pizza_type> pizza
214	1	change		can i get a different crust type, please
215	1	hello			hello,
216	1	order			order my
217	1	inform			it is <user_phone>425-555-1276</user_phone>
218	1	confirm, inform	yep, oh, and can you, please make it <pickup_delivery>delivery</pickup_delivery>
219	1	inform			<pickup_delivery>pick up</pickup_delivery> will be great, please
220	1	order, inform		please order my <pizza_type>previous</pizza_type>
221	1	confirm		yeah,
222	1	order, inform		hello, can i get a <pizza_type>pepperoni</pizza_type>, please ?
223	1	inform			please make it <pickup_delivery>take-out</pickup_delivery>
224	1	order, inform		could i place an order for my <pizza_type>favorite</pizza_type>
225	1	order, inform		i'll have a <pizza_type>4 cheese</pizza_type>
226	1	deny			nope
227	1	reorder, inform	please order my <pizza_type>most frequent</pizza_type> pizza again
228	1	confirm		yeah, let's go with pick up
229	1	confirm		hello,
230	1	reorder, inform	place another order for my <pizza_type>previous</pizza_type> order again
231	1	confirm, inform	yep, how about <pickup_delivery>to go</pickup_delivery>, please
232	1	goodbye		thanks
233	1	inform			<pickup_delivery>take out</pickup_delivery> sounds good
234	1	inform			it's <user_name>danko</user_name>, please
235	1	confirm		yeah, absolutely
236	1	rerder, inform		please place another order for my <pizza_type>previous</pizza_type> order again
237	1	inform			let's go with <pickup_delivery>delivery</pickup_delivery>
238	1	goodbye		thanks
239	1	inform			<pickup_delivery>pick up</pickup_delivery> would be great, please
240	1	order, inform		hello, may i place an order for <pickup_delivery>delivery</pickup_delivery>
241	1	goodbye		super thank you
242	1	reorder		i'd like to reorder my order
243	1	reorder, inform	could i order my <pizza_type>previous</pizza_type> pizza order, please
244	1	inform			how about <pickup_delivery>pick up</pizza_type>, please
245	1	query_status		can you look up my order?
246	1	query_status		where's the pizza i ordered?
247	1	inform			<user_phone>360-555-5552</user_phone>
248	1	order, inform		hi, i'd like a <pickup_delivery>delivery</pickup_delivery> order
249	1	reorder, inform	get my <pizza_type>previous</pizza_type> order
250	1	order, inform		give me a <pizza_type>pepperoni</pizza_type>. make it <pizza_size>large</pizza_size>
251	1	query_status		where's the pizza i ordered?
252	1	order, inform		hey, how's it going? may i place an order for <pickup_delivery>pick up</pickup_delivery>, please
253	1	deny, change		no, i'd like to change something
254	1	goodbye		super thanks
255	1	order			i'd like to place an order
256	1	change		actually i'd rather get a different kind of crust
257	1	order, inform		i'll have a <pizza_type>vegetarian</pizza_type>, please. make it <pizza_size>10 inch</pizza_size>
258	1	inform			<pickup_delivery>to go</pickup_delivery> would be great
259	1	reorder, inform	get my <pizza_type>most common</pizza_type> order
260	1	order, inform		may i get a <pizza_size>12 inch</pizza_size> <pizza_type>pepperoni</pizza_type>, please
261	1	confirm		yep, take out would be great
262	1	reorder		could i place another order for my order again
263	1	reorder, inform	may i get my <pizza_type>most common</pizza_type> pizza, please
264	1	reorder, inform	could i place an order for my <pizza_type>previous</pizza_type> pizza order again
265	1	inform			let's go with <pickup_delivery>pick up</pickup_delivery>
266	1	inform			let's go with <user_name>ali</user_name>, please
267	1	order, inform		i'll get a <pizza_type>pepperoni</pizza_type> on <pizza_crust>regular</pizza_crust>, please. make it <pizza_size>14 inch</pizza_size>
268	1	inform			it's <user_name>zhaleh</user_name>
269	1	inform			<pickup_delivery>pick up</pickup_delivery>, please
270	1	reorder, inform	please place another order for my preferred pizza
271	1	query_status		how long until my order gets delivered?
272	1	change	no, i'd like to change something
273	1	query_status	how long until my order arrives?
274	1	inform		i'll get a <pizza_type>all the veggies<pizza_type>
275	1	inform		place another order for my <pizza_type>most recent<pizza_type> again
276	1	inform		i'll get a <pizza_size>12 inch</pizza_size> <pizza_type>hawaiian</pizza_type>, please
277	1	inform		could i place an order for my <pizza_type>preferred</pizza_type> order
278	1	deny, change	nope, i'd like to change something
279	1	inform		it is <user_phone>360-555-7000</user_phone>
280	1	inform		<pickup_delivery>take-out</pickup_delivery> from your nearest store
281	1	hello		hey, how's it going?
282	1	inform		i want to order a <pizza_type>veggie supreme</pizza_type> on <pizza_crust>regular</pizza_crust> crust
283	1	inform		let's go with <user_name>logan</user_name>
284	1	inform		it's <user_name>jp</user_name>
285	1	inform		could i reorder my <pizza_type>previous</pizza_type> order, please
286	1	inform		i want a <pizza_type>4 cheese</pizza_type>
287	1	inform		please make it <pickup_delivery>pick up</pickup_delivery>, please
288	1	inform		how about <pickup_delivery>take out</pickup_delivery>
289	1	goodbye	thanks
290	1	inform		may i get a <pizza_size>12 inch</pizza_size> <pizza_type>vegetarian</pizza_type>, please
291	1	inform		let's go with <user_name>elizabeth</user_name>, please
292	1	inform		i'll order a <pizza_size>medium</pizza_size> <pizza_type>pepperoni</pizza_type> on <pizza_crust>deep dish</pizza_crust>, please
293	1	inform		i'll have a <pizza_type>veggie supreme</pizza_type>. make it <pizza_size>small</pizza_size>, please
294	1	inform		it is <user_phone>425-555-8789</user_phone>
295	1	confirm	yep, that works
296	1	confirm, inform	yeah, <pickup_delivery>pick up</pickup_delivery> will be great, please
297	1	confirm	that would be great yep,
298	1	confirm, inform	yeah, <pickup_delivery>take-out</pickup_delivery> would be great, please
299	1	hello		hello,
300	1	inform		could i place an order for my <pizza_type>most frequent</pizza_type> pizza again, please
301	1	goodbye	thanks
302	1	inform		use <user_name>paul</user_name>
303	1	inform		can i place another order for my <pizza_type>preferred</pizza_type> again, please
304	1	inform		<pickup_delivery>pick up</pickup_delivery>, please
305	1	query_status	how long until i get my order ?
306	1	inform		please reorder my <pizza_type>previous</pizza_type>
307	1	confirm, inform	yep, <pickup_delivery>take out</pickup_delivery> would be great, please
308	1	inform		hello, i'll have a <pizza_type>4 cheese</pizza_type>, please
309	1	deny		nope
310	1	inform		may i place an order for <pickup_delivery>take out</pickup_delivery>, please
311	1	inform		i'll have a <pizza_size>large</pizza_size> <pizza_type>meat lovers</pizza_type>
312	1	deny, change	no, i'd like to change something
313	1	inform		it's <user_name>elizabeth</user_name>, please
314	1	inform		reorder my <pizza_type>most recent</pizza_type> pizza
315	1	hello		hi,
316	1	inform		please get my <pizza_type>most frequent</pizza_type> pizza order
317	1	confirm	yep, perfect
318	1	deny, change	nope, i'd like to change something
319	1	inform		hello, i'd like to order for <pickup_delivery>take-out</pickup_delivery>
320	1	goodbye	awesome thank you
321	1	inform		it's <user_name>elizabeth</user_name>, please
322	1	inform		hey, how's it going? may i place an order for <pickup_delivery>pick up</pickup_delivery>, please
323	1	inform		place another order for my <pizza_type>most common</pizza_type> pizza order
324	1	inform		<pickup_delivery>take out</pickup_delivery> will be great, please
325	1	inform		let's go with <user_name>eva</user_name>, please
326	1	deny, change	nope, i'd like to change something
327	1	inform		can i get a <pizza_type>meat lovers</pizza_type> on <pizza_crust>thin</pizza_crust>, please
328	1	query_status	how long until my pizza order gets delivered?
329	1	deny		nope
330	1	order		hello, i'd like to place an order
331	1	inform		use <user_name>alex</user_name>, please
332	1	inform		may i order a <pizza_type>4 cheeses</pizza_type> on <pizza_crust>deep dish</pizza_crust>, please. make it <pizza_size>14 inch</pizza_size>
333	1	inform		hi, i'd like a <pizza_type>veggie supreme</pizza_type>, please
334	1	inform		can i place an order for my <pizza_type>usual</pizza_type> again, please
335	1	inform		yeah, <pickup_delivery>take out</pickup_delivery> will be great, please
336	1	deny, change	no, i'd like to change something
337	1	inform		<pickup_delivery>take-out</pickup_delivery> would be great, please
338	1	confirm, inform	yep, <pickup_delivery>take-out</pickup_delivery> would be great, please
339	1	hello		hi,
340	1	confirm	yes, that works
341	1	inform		place another order for my <pizza_type>most frequent</pizza_type> pizza
342	1	inform		may i reorder my <pizza_type>previous</pizza_type> order
343	1	inform		please get my <pizza_type>most recent</pizza_type> pizza again
344	1	inform		i'll get a <pizza_type>all cheese</pizza_type>
345	1	change	i'd rather get a <pizza_size>10 inch</pizza_size> <pizza_type>pepperoni</pizza_type> instead
346	1	inform		i'd like to order a <pizza_type>vegan</pizza_type>, please
347	1	inform		i want a <pizza_type>4 cheese</pizza_type> on <pizza_crust>regular</pizza_crust> crust. make it <pizza_size>10 inch</pizza_size>
348	1	inform		order my <pizza_type>favorite</pizza_type> order
349	1	inform		how about <pickup_delivery>take-out</pickup_delivery>
350	1	inform		make it  <pickup_delivery>take out</pickup_delivery> from your closest location
351	1	query_status	is my order ready?
352	1	deny, change	nope, i'd like to change something
353	1	goodbye	awesome thank you
354	1	goodbye	great thank you
355	1	inform		could i order my <pizza_type>favorite</pizza_type> order
356	1	inform		may i place another order for my <pizza_type>preferred</pizza_type>, please
357	1	deny		nope
358	1	inform		can i get a <pizza_type>pepperoni</pizza_type> on <pizza_crust>gluten free</pizza_crust> crust
359	1	inform		may i get a <pizza_type>the meats</pizza_type> on <pizza_crust>gluten free</pizza_crust>. make it <pizza_size>medium</pizza_size>, please
360	1	confirm, inform	yes, <pickup_delivery>pick up</pickup_delivery> will be great
361	1	deny, change	no, i'd like to change something
362	1	inform		i'd like to order my <pizza_type>usual</pizza_type> pizza order again, please
363	1	reorder	can i order my again
364	1	confirm, inform	yes, <pickup_delivery>delivery</pickup_delivery> sounds good
365	1	deny		nope
366	1	confirm, inform	yes, oh, and can you, please make it <pickup_delivery>delivery</pickup_delivery>, please
367	1	inform		please place an order for my <pizza_type>preferred</pizza_type> pizza
368	1	inform		use <user_name>david</user_name>, please
369	1	inform		give me a <pizza_type>all the veggies</pizza_type>. make it <pizza_size>large</pizza_size>
370	1	inform		it's <user_name>alexandre</user_name>
371	1	hello		hello,
372	1	inform		<pickup_delivery>take out</pickup_delivery>, please
373	1	inform		place an order for my <pizza_type>most frequent</pizza_type> order
374	1	deny		no
375	1	inform		<pickup_delivery>take out</pickup_delivery> will be great, please
376	1	inform		oh, and can you, please make it <pickup_delivery>pick up</pickup_delivery> from your seattle shop, please
377	1	inform		let's go with <pickup_delivery>take-out</pickup_delivery>, please
378	1	goodbye	super thanks
379	1	inform		could i reorder my <pizza_type>previous</pizza_type> pizza order again
380	1	query_status	can you tell me where's my pizza ?
381	1	inform		please make it <pickup_delivery>take out</pickup_delivery>, please
382	1	inform		i'd like to order my <pizza_type>most common</pizza_type> pizza
383	1	inform		it's <user_phone>360-555-1271</user_phone>
384	1	inform		<pickup_delivery>take out</pickup_delivery> sounds good
385	1	inform		<pickup_delivery>pick up</pickup_delivery> sounds good
386	1	goodbye	amazing thanks
387	1	inform		oh, and can you, please make it <pickup_delivery>delivery</pickup_delivery>
388	1	inform		<user_phone>360-555-7000</user_phone>
389	1	inform		i want a <pizza_type>pepperoni</pizza_type> on <pizza_crust>gluten free</pizza_crust>. make it <pizza_size>small</pizza_size>
390	1	inform		may i place an order for <pickup_delivery>take out</pickup_delivery>
391	1	goodbye	super thanks
392	1	deny		no
393	1	inform		please reorder my <pizza_type>favorite</pizza_type> order
394	1	reorder	get my pizza order again
395	1	inform		i'll get a <pizza_type>meat lovers</pizza_type> on <pizza_crust>regular</pizza_crust>. make it <pizza_size>large</pizza_size>
396	1	hello		hello,
397	1	inform		may i reorder my <pizza_type>preferred</pizza_type> order again, please
398	1	reorder	may i place an order for my pizza, please
399	1	deny		no
