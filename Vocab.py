
import string

class labelVocab:
    """
    This class prepares labels.

    """
    def __init__(self, name):
        self.name = name
        self.label2index = {}
        self.index2label = {}
        self.n_labels = 0

    def addLabel(self, labels):
        # Update max length
        for idx,label in enumerate(labels):
            self.label2index[label] = idx
            self.index2label[idx] = label
        self.n_labels = len(labels)

class wordVocab:
    def __init__(self, name):
        self.name = name
        self.word2index = {'UNK': 0}
        self.index2word = {0: "UNK"}
        self.n_words = 1
        self.max_len = 0 

    def addSentence(self, sentence):

        # Update max length
        l = len(sentence)
        if l>self.max_len:
            self.max_len = l

        for word in sentence:
            self.addWord(word)

    def addWord(self, word):
        if word not in self.word2index:
            self.word2index[word] = self.n_words
            self.index2word[self.n_words] = word
            self.n_words += 1

class charVocab:
    def __init__(self, name):
        self.name = name
        self.char2index = {'~':0}
        self.index2char = {0:'~'}

        self.vocab_set = \
            list(string.ascii_lowercase+string.digits+string.punctuation)
        self.vocab_set.remove('~')

        # Init
        for i,c in enumerate(self.vocab_set):
            self.char2index[c] = i+1 
            self.index2char[i+1] = c

        self.n_words = len(self.vocab_set)
        self.max_len = 1

    def addSentence(self, sentence):

        sentence = list(' '.join(sentence))

        # Update max length
        l = len(sentence)
        if l>self.max_len:
            self.max_len = l



